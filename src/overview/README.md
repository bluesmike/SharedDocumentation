# Overview

All LibreFoodPantry projects can be found under [LibreFoodPantry](https://gitlab.com/LibreFoodPantry) on GitLab.


## Client Projects

There are several client projects that are each being developed in response
to the needs for a specific client.

- NCC-NEST-Registrations - Nassau Community College Food Pantry Guest Registration
- NEST - Nassau Community College Food Pantry App
- BEAR-Necessities-Market - Web app for ordering food from a food pantry.
- FoodKeeper-API - API for USDA FSIS FoodKeeper data (https://catalog.data.gov/dataset/fsis-foodkeeper-data)
- Theas-Pantry - Thea's Pantry at Worcester State University

These are being used to better understand each clients needs.
What we learn from these projects will be aggregated into a more general
set of requirements and will be used to build a more general purpose solution.
(See Story Map below.)


## Infrastructure Projects

There are also several infrastructure projects that provide various resources
for organizing and coordinating LibreFoodPantry.

- Community - Where we organize and connect.
- librefoodpantry.org - Website for librefoodpantry.org
- SharedDocumentation - Common documentation for all LFP projects.
- StoryMap - Manage and coordinate around the story map.

Documentation for some of these projects can be found on this site.
At the time of writing, not all projects have integrated their documentation
with this site. For those projects, their documentation often begins in
their `README.md` and `CONTRIBUTING.md` files, and continues in their `doc/`
or `docs/` folder.


## Story Map

Below is an initial vision of a more general project. It will be updated
and elaborated over time. This vision below is in the form of a story map
(see [The New User Story Backlog is a Map](https://www.jpattonassociates.com/the-new-backlog/) for more about the purpose and structure of a story map).

[![Product story map](https://gitlab.com/LibreFoodPantry/StoryMap/raw/master/product-story-map.png)](https://gitlab.com/LibreFoodPantry/StoryMap/raw/master/product-story-map.png)


---
Copyright &copy; 2019 The LibreFoodPantry Authors. This work is licensed
under the Creative Commons Attribution-ShareAlike 4.0 International License.
To view a copy of this license, visit
http://creativecommons.org/licenses/by-sa/4.0/.
