# Generating the AUTHORS File

Use `list-authors.py` to generate the AUTHORS file as follows.

```bash
$ bin/list-authors.py > AUTHORS
```

`list-authors.py` generates the list of authors based on `Author`
and `Co-Authored-By` lines in the project's commit messages.


---
Copyright &copy; 2019 The LibreFoodPantry Community. This work is licensed
under the Creative Commons Attribution-ShareAlike 4.0 International License.
To view a copy of this license, visit
http://creativecommons.org/licenses/by-sa/4.0/.
