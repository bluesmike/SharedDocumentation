# Changelog


## 2019-09-25

### Added
- This title page with a changelog.

### Changed
- The menuing to make guides more modular and consistent.
- The file structure to make their design more consistent.


---
Copyright &copy; 2019 The LibreFoodPantry Authors. This work is licensed
under the Creative Commons Attribution-ShareAlike 4.0 International License.
To view a copy of this license, visit
http://creativecommons.org/licenses/by-sa/4.0/.
