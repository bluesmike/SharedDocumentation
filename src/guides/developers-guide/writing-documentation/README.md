# Writing Documentation

All LibreFoodPantry documentation is written in Markdown. There are several "flavors" of Markdown. So your next question should be, "Which flavor of Markdown?"

## Which Flavor of Markdown?

> If you are new to Markdown, start with [CommonMark](https://commonmark.org/). And don't worry. The different flavors have more in common than they have differences. It's only when you try to get fancy that you need to learn special features that each flavor offers.


To answer this question, we need a little more information about our documentation system. We use
[Docsify](https://docsify.js.org/#/?id=docsify) to provide a website based on Markdown files. Docsify
uses [marked](https://marked.js.org/#/README.md#README.md) to render Markdown to HTML. Marked supports the following flavors:

- The original [markdown.pl](https://daringfireball.net/projects/markdown/)
- [CommonMark](https://commonmark.org/)
- [GitHub Flavored Markdown](https://guides.github.com/features/mastering-markdown/)

So, those are the flavors available to us when we write our documentation. In general, stay away from special features in a particular flavor unless you really need it. That way, if we ever decide to change flavors, porting will require less work.


## File structure

Example file structure for `example-project`
```
/docs
    _sidebar.md
    README.md
    /user
        README.md
        /install
            README.md
            screenshot1.png
            screenshot2.png
        /run
            README.md
    /dev
        README.md
        /setup
            README.md
        /test
            README.md
```

- Place all project documentation in the `/docs` subdirectory in the root of
  the project's repository.
- All `.md` files are written in Markdown format.
- `_sidebar.md` provides a left navigation bar for each page in this project's
  documentation site.
- All text content is placed in `README.md` files.
- The `/docs/README.md` is the homepage for this documentation site.
- Each directory with a `README.md` represents and single page in the site. The
  name of the directory should correspond to the name of the page.
- Place image files and other auxiliary files in the page directory that
  references them.


## The `_sidebar.md` file

These provide the left sidebar menu that visitors use to navigate your site's
documentation. This file is include in each page served within your site.  This
ensures that every page in your site has a left sidebar menu.

The file is in Markdown format, and contains a single, possibly nested, list of
links.

Example `_sidebar.md` for `example-project`
```markdown
- [Home](/projects/example-project/README.md)
- Getting Started
  - [Install](/projects/example-project/user/install/README.md)
  - [Run](/projects/example-project/user/run/README.md)
- Developer's Guide
  - [Setup](/projects/example-project/developer/setup/README.md)
  - [Test](/projects/example-project/developer/test/README.md)
```

All links in `_sidebar.md` must be absolute path links. That is, they must
start with `/` and provide the full path to the file in the ***website's
directory structure***.

Note that the contents of your projects `/docs/` directory are copied into a
directory `/projects/<project-name>/`, where `<project-name>` is the name of
your project (it's in the URL of the GitLab URL to your project). So the absolute path
in the ***website's directory structure*** to the root of your project's
documentation is `/projects/<project-name>/`.

For example, suppose your project's name is `BEAR-Necessities-Market`. And
suppose it has a file in its repository `/docs/howto/install-flask/README.md`.
To link to it from within `/docs/_sidebar.md`, we would use the following link
absolute path link
`/projects/BEAR-Necessities-Market/howto/install-flask/README.md`.


## The `README.md` files

- These contain the textual content of your pages.
- They are written in Markdown.
- Links
    - Generally use relative path links to refer to other files within the same
  project's documentation. For example, to refer to an image in the same
  directory, simply use `![Awesome Image](./image-name.png)`. These links should
  work both when viewed on the webpage as well as when viewed on GitLab.
    - Absolute path links will not work on GitLab; or if they do, they won't work
  on the website. When you must choose, make it work on the website.
    - Link to other projects' documentation using the absolute URL to their page on librefoodpantry.org, not the URL to their page in their project's repository.
