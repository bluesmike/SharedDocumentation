# Communication


## Issue Trackers

Most communication takes place on issues in our issue trackers. Use our issue
trackers to:

- Ask and answer questions.
- Report and fix bugs.
- Request new features and help design them.
- Start and participate in discussions.
- Provide feedback on all of the above.
- Coordinate with other developers.

To do any of the above, visit the
[LibreFoodPantry group](https://gitlab.com/LibreFoodPantry/) on GitLab,
add select a project that most closely matches your issue.
If no project matches or you aren't sure which is best,
select [LibreFoodPantry/Community](https://gitlab.com/LibreFoodPantry/Community).
Then select `Issues` in the left menu. And then `New Issue`.

Be sure to customize your
[GitLab notification emails](https://docs.gitlab.com/ee/workflow/notifications.html)
so that you stay informed about changes groups, projects,
and/or issues that you are interested in.

Note that LibreFoodPantry projects are configured to send event notifications
to #notification channels in Discord
(see the section on Discord for more information).
So that is another option to stay aware of what's going on.


## Discord

Discord is a powerful communication tool marketed towards gamers.
But you don't have to be a gamer to use it, and we use it for social good!

Please use our Discord server to:

- Find mentors.
- Ask questions and get help.
- Chat with other community members.
- Organize and participate in meetings.
- Stay current with changes in the community and its projects.

If you are new to Discord, see our
[Discord tips and configuration](discord-tips-and-configuration.md).

[Join us on Discord](https://discord.gg/PRth8YK)

## Google Groups (email)

* LibreFoodPantry@googlegroups.com
    * General announcements; project-wide communication
    * [Sign up here](https://groups.google.com/forum/#!forum/librefoodpantry)
* LibreFoodPantry-Coordinating-Committee@googlegroups.com
    * Announcements and communication within the committee.
* LibreFoodPantry-Trustees@googlegroups.com
    * Announcements and communication within the trustees.
* LibreFoodPantry-Accounts@googlegroups.com
    * Any accounts owned by LibreFoodPantry.


---
Copyright &copy; 2019 The LibreFoodPantry Authors. This work is licensed
under the Creative Commons Attribution-ShareAlike 4.0 International License.
To view a copy of this license, visit
http://creativecommons.org/licenses/by-sa/4.0/.
